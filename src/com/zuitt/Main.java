package com.zuitt;

public class Main {

    public static void main(String[] args){

        User user1 = new User("Tee Jae", "Calinao", 25, "Antipolo City");

        Course course1 = new Course("Physics 101", "Learn Physics", 30, 1500.00, "July 1,2022", "October 31, 2022", user1);

        user1.userInfo();
        course1.courseInfo();
    }

}
