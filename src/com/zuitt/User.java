package com.zuitt;

public class User {

    //properties
    public String firstName;
    private String lastName;
    private int age;
    private String address;

    //constructor
    public User(){};

    //parameterized constructor
    public User(String firstName, String lastName, int age, String address){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.address = address;
    }

    //getter
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    //setter
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //methods
    public void userInfo(){
        System.out.println("User's first name: " + getFirstName());
        System.out.println("User's last name: " + getLastName());
        System.out.println("User's age: " + getAge());
        System.out.println("User's address: " + getAddress());
    }
}
